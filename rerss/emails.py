import smtplib
import ssl
from email.message import EmailMessage

from . import config


ACCESS_GRANTED = """Hi {username},

You've been granted an access to the rerss awesome project, please click the following link:

{link}

The rerss Team
"""


def send(to, subject, body):
    msg = EmailMessage()
    msg.set_content(body)
    msg["Subject"] = subject
    msg["From"] = config.FROM_EMAIL
    msg["To"] = to
    if not config.SEND_EMAILS:
        return print("Sending email", str(msg))
    context = ssl.create_default_context()
    with smtplib.SMTP(config.SMTP_HOST, config.SMTP_PORT) as server:
        if config.SMTP_TLS:
            server.starttls(context=context)
        try:
            if config.SMTP_LOGIN:
                server.login(config.SMTP_LOGIN, config.SMTP_PASSWORD)
            server.send_message(msg)
        except smtplib.SMTPException as err:
            raise RuntimeError from err
        else:
            print(f"Email sent to {to}: {subject}")
