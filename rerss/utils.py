"""Utils should have no internal dependencies"""
import json
from datetime import date, datetime, timezone
from time import mktime


def ts_to_dt(ts):
    return datetime.fromtimestamp(mktime(ts), tz=timezone.utc)


def utcnow():
    return datetime.now(timezone.utc)


def default_json(v):
    if isinstance(v, (datetime, date)):
        return v.isoformat()
    return str(v)


def json_dumps(v):
    return json.dumps(v, default=default_json, indent=None)


def nl2br(s):
    return "<br />\n".join(s.split("\n"))
