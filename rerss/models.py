from . import utils


class Base(dict):
    def __getattr__(self, attr):
        return self.get(attr)

    def __setattr__(self, key, attr):
        self[key] = attr

    def __repr__(self):
        return '<{} "{}">'.format(self.__class__.__name__, str(self)[:50])


class Feed(Base):
    def __str__(self):
        return self.title or ""

    @classmethod
    def from_parser(cls, data):
        inst = cls()
        inst.id = data["id"]
        inst.link = data["link"]
        inst.title = data.get("title", "missing title fixme")
        inst.subtitle = data.get("subtitle", "missing subtitle fixme")
        updated = data.get("updated_parsed")
        if updated:
            updated = utils.ts_to_dt(updated)
        else:
            updated = utils.utcnow()
        inst.updated = updated
        inst.failed = None
        inst.active = True  # Needed to make Elm happy. TODO remove.
        return inst


class Entry(Base):
    def __str__(self):
        return self.title or ""

    @classmethod
    def from_parser(cls, data):
        updated = data.pop("updated_parsed", None) or data.pop("published_parsed")
        updated = utils.ts_to_dt(updated)
        image = None
        for link in data["links"]:
            if link.get("type", "").startswith("image"):
                image = link["href"]
        content = data.pop("content", None)
        if content:
            content = content[0]["value"]  # Deal with more than one content tag.
            if "&lt;" not in content:
                content = utils.nl2br(content)
        author = dict(data.get("author_detail", {}))
        summary = data.get("summary") or data.get("description")
        if summary and "&lt;" not in summary:
            summary = utils.nl2br(summary)
        return cls(
            data,
            content=content,
            updated=updated,
            image=image,
            raw=data,
            author=author,
            summary=summary,
        )
