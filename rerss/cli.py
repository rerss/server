import os
import sys
from functools import wraps
from pathlib import Path
from xml.etree import ElementTree

from minicli import cli, run
from roll.extensions import logger, simple_server, static

from rerss import db, helpers, session, loggers
from rerss.app import app


def iter_db(view):
    @wraps(view)
    async def wrapper(*args, **kwargs):
        for path in helpers.iter_db():
            if not (path / "rerss.db").exists():
                print(f"No DB, skipping {path}")
                continue
            print(f"Loading {path}")
            with session.as_user(path.name):
                db.init()
                await view(*args, **kwargs)

    return wrapper


@cli
def serve(log=False, reload=False):
    if log:
        loggers.init()
        logger(app)
    if reload:
        import hupper

        hupper.start_reloader("rerss.cli.serve")
    static(app, prefix="/app", root="rerss/app")
    try:
        port = int(os.environ.get("PORT"))
    except (ValueError, TypeError):
        port = 1707
    host = os.environ.get("HOST", "0.0.0.0")
    simple_server(app, port=port, host=host)


@cli(name="sync")
@iter_db
async def sync():
    print("Syncing feeds")
    for feed in db.feed.select(where=["active=true"]):
        print("Syncing", feed["link"])
        await helpers.fetch(feed["link"])
        print("Done", feed["link"])


@cli
@iter_db
async def make_feeds():
    helpers.make_feed()


@cli
def bootstrap_for(username, email):
    helpers.bootstrap_for(username, email)


@cli
@iter_db
async def import_opml(opml_file: Path):
    """Import feeds from an opml file."""
    if not opml_file.exists():
        sys.exit("Error: file not found")
    with opml_file.open() as f:
        tree = ElementTree.parse(f)
    for node in tree.findall(".//outline"):
        url = node.attrib.get("xmlUrl")
        if url:
            await helpers.fetch(url)
            print("Imported feed", url)
    print("Done")


@cli
@iter_db
async def make_opml():
    """Exports feeds to an opml file."""
    helpers.make_opml()


if __name__ == "__main__":
    run()
