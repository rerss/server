from datetime import timedelta
from functools import wraps

import jwt
from roll import HttpError

from . import config, utils


def create(username, email):
    return jwt.encode(
        {
            "sub": str(username),
            "email": str(email),
            "exp": utils.utcnow() + timedelta(weeks=52),
        },
        config.SECRET,
        config.JWT_ALGORITHM,
    )


def read(token):
    try:
        decoded = jwt.decode(token, config.SECRET, algorithms=[config.JWT_ALGORITHM])
    except (jwt.DecodeError, jwt.ExpiredSignatureError):
        raise ValueError
    return decoded["sub"], decoded["email"]


def require(view):
    @wraps(view)
    def wrapper(request, response, *args, **kwargs):
        token = request.headers.get("API-KEY") or request.cookies.get("api-key")
        if not token:
            raise HttpError(401, "No authentication token was provided.")
        try:
            username, _ = read(token)
        except ValueError:
            raise HttpError(401, "Invalid token")
        request["username"] = username.lower()
        return view(request, response, *args, **kwargs)

    return wrapper
