import logging

logger = logging.getLogger("rerss")


def init(level=logging.DEBUG):
    logger.setLevel(level)
    logger.addHandler(logging.StreamHandler())
