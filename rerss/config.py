import os
from pathlib import Path

DATA_ROOT = "./db/"
BASE_URL = "http://0.0.0.0:1707"
DOMAIN = "rerss.org"
SECRET = "sikretfordevonly"
JWT_ALGORITHM = "HS256"
SEND_EMAILS = False
SMTP_HOST = "127.0.0.1"
SMTP_PORT = 1025
SMTP_PASSWORD = ""
SMTP_LOGIN = ""
SMTP_TLS = False
FROM_EMAIL = "rerss <contact@example.com>"
SITE_DESCRIPTION = "rerss"
EMAIL_SIGNATURE = "rerss"
ALLOW_ORIGIN = "*"


def init():
    for key, value in globals().items():
        if key.isupper():
            env_key = "RERSS_" + key
            typ = type(value)
            if typ in (list, tuple, set):
                real_type, typ = typ, lambda x: real_type(x.split(","))
            if env_key in os.environ:
                globals()[key] = typ(os.environ[env_key])
    Path(DATA_ROOT).mkdir(exist_ok=True)


def debug():
    for key, value in globals().items():
        if not key.isupper():
            continue
        print(f"{key}={value}")


init()
