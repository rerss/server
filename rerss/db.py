import json
import sqlite3
from datetime import datetime, timezone
from pathlib import Path

from feedparser.util import FeedParserDict

from . import config, loggers, models, session, utils

CONN = None

BOOTSTRAP = [
    """CREATE TABLE IF NOT EXISTS feed
    (id TEXT PRIMARY KEY, title TEXT, subtitle TEXT, link TEXT UNIQUE NOT NULL,
    updated TIMESTAMP, synced TIMESTAMP,
    active BOOLEAN NOT NULL ON CONFLICT REPLACE DEFAULT true, failed TIMESTAMP)""",
    """CREATE TABLE IF NOT EXISTS entry (id TEXT PRIMARY KEY, raw JSON, title TEXT,
    summary TEXT, content TEXT, link TEXT UNIQUE NOT NULL, image TEXT, author JSON,
    source JSON, updated TIMESTAMP, flagged BOOLEAN NOT NULL DEFAULT false,
    seen BOOLEAN NOT NULL DEFAULT false, bookmark BOOLEAN NOT NULL DEFAULT false)""",
    """CREATE TABLE IF NOT EXISTS entry_feed
    (entry TEXT, feed TEXT,
    FOREIGN KEY(entry) REFERENCES entry(link) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY(feed) REFERENCES feed(link) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (entry, feed))
    """,
]

sqlite3.register_adapter(bool, int)
sqlite3.register_converter("BOOLEAN", lambda v: bool(int(v)))
sqlite3.register_adapter(datetime, lambda v: datetime.timestamp(v))
sqlite3.register_converter(
    "TIMESTAMP", lambda v: datetime.fromtimestamp(float(v), tz=timezone.utc)
)
sqlite3.register_adapter(dict, utils.json_dumps)
sqlite3.register_adapter(FeedParserDict, utils.json_dumps)
sqlite3.register_converter("JSON", json.loads)


def init():
    username = session.username.get()
    db_path = Path(config.DATA_ROOT) / username / "rerss.db"
    loggers.logger.info(f"Init db for {db_path}")
    global CONN
    CONN = sqlite3.connect(
        db_path, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES
    )
    CONN.execute("PRAGMA foreign_keys = ON;")
    CONN.row_factory = sqlite3.Row


def bootstrap():
    with CONN:
        for sql in BOOTSTRAP:
            CONN.execute(sql)


class NoData(ValueError):
    pass


class table:
    @classmethod
    def execute(cls, sql, *params):
        loggers.logger.debug(sql)
        loggers.logger.debug(params)
        with CONN:
            cur = CONN.cursor()
            cur.execute(sql, params)
            return cur

    @classmethod
    def fetchmany(cls, sql, *params):
        loggers.logger.debug(sql)
        with CONN:
            for row in CONN.execute(sql, params):
                yield cls.factory(row)

    @classmethod
    def fetchone(cls, sql, *params):
        loggers.logger.debug(sql)
        loggers.logger.debug(params)
        with CONN:
            cur = CONN.cursor()
            cur.execute(sql, params)
            row = cur.fetchone()
            if not row:
                raise NoData
            return cls.factory(row)

    @classmethod
    def fetchval(cls, sql, *params):
        with CONN:
            cur = CONN.cursor()
            cur.execute(sql, params)
            row = cur.fetchone()
            if not row:
                raise NoData
            return row[0]

    @classmethod
    def upsert(cls, inst):
        columns = []
        values = []
        on_conflict = []
        table = cls.__name__
        for key in cls.columns:
            if key in inst:
                columns.append(key)
                values.append(inst[key])
                on_conflict.append(f"{key}=?")
        placeholders = ",".join("?" * len(values))
        columns = ",".join(columns)
        on_conflict = ",".join(on_conflict)
        values = values + values  # For placeholders AND for ON CONFLICT SET
        return cls.fetchone(
            f"INSERT INTO {table}({columns}) VALUES ({placeholders}) "
            f"ON CONFLICT(link) DO UPDATE SET {on_conflict} "
            "RETURNING *",
            *values,
        )

    @classmethod
    def factory(cls, data):
        return cls.model(data)


class feed(table):
    model = models.Feed
    columns = [
        "id",
        "title",
        "subtitle",
        "link",
        "updated",
        "synced",
        "active",
        "failed",
    ]

    @classmethod
    def all(cls):
        return cls.fetchmany("SELECT * FROM feed ORDER BY title ASC")

    @classmethod
    def select(cls, where="", params=(), limit=None, offset=None):
        if where:
            if not isinstance(where, list):
                where = [where]
            where = " WHERE " + " AND ".join(where)
        return cls.fetchmany(f"SELECT * FROM feed {where} ORDER BY title ASC", *params)

    @classmethod
    def insert(cls, feed):
        return cls.upsert(feed)

    @classmethod
    def delete(cls, link):
        cls.execute("DELETE FROM feed WHERE link=?", link)

    @classmethod
    def update_link(cls, old, new):
        cls.execute("UPDATE feed SET link=? WHERE link=?", new, old)

    @classmethod
    def from_links(cls, links):
        where = "link IN ({})".format(",".join("?" * len(links)))
        return [dict(f) for f in feed.select(where=[where], params=links)]


class entry(table):
    model = models.Entry
    columns = [
        "id",
        "title",
        "summary",
        "content",
        "link",
        "image",
        "author",
        "updated",
        "raw",
        "source",
        "flagged",
        "seen",
        "bookmark",
    ]

    @classmethod
    def insert(cls, entry):
        return cls.upsert(entry)

    @classmethod
    def select(cls, where=None, params=None, limit=None, offset=None):
        if where:
            where = " WHERE " + " AND ".join(where)
        else:
            where = ""
        rows = cls.fetchmany(
            "SELECT entry.*, GROUP_CONCAT(entry_feed.feed, '#') AS sources "
            "FROM entry JOIN entry_feed ON entry.link = entry_feed.entry "
            f"{where} GROUP BY entry.link ORDER BY updated DESC LIMIT ? OFFSET ?",
            *params,
            limit,
            offset,
        )
        for row in rows:
            row = dict(row)
            sources = row["sources"].split("#")
            row["sources"] = feed.from_links(sources)
            yield row

    @classmethod
    def flagged(cls):
        return cls.fetchmany(
            "SELECT * FROM entry WHERE flagged=1 ORDER BY updated DESC LIMIT 20"
        )

    @classmethod
    def fetchone(cls, sql=None, *params):
        if not sql:
            sql = "SELECT * FROM entry WHERE link=?"
        return super().fetchone(sql, *params)

    @classmethod
    def factory(cls, data):
        inst = super().factory(data)
        if "sources" not in inst:
            inst["sources"] = entry_feed.feeds_for(inst.link)
        return inst


class entry_feed(table):
    @classmethod
    def insert(cls, entry, feed):
        cls.execute(
            "INSERT OR IGNORE INTO entry_feed VALUES (?, ?)", entry.link, feed.link
        )

    @classmethod
    def feeds_for(cls, link):
        cursor = cls.execute("SELECT feed FROM entry_feed WHERE entry=?", link)
        return feed.from_links([r[0] for r in cursor.fetchall()])
