import contextvars
from contextlib import contextmanager

from rerss.loggers import logger

username = contextvars.ContextVar("username")


@contextmanager
def as_user(name):
    old = username.get(None)
    username.set(name)
    logger.debug(f"Set session to {name} (was {old})")
    yield
    logger.debug(f"Set back session to {old}")
    username.set(old)
