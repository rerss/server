"""Helpers have internal dependencies"""
import re
from pathlib import Path

import feedparser
import httpx
import xmltodict
from cssselect import HTMLTranslator
from lxml.html import fromstring, tostring
from lxml.html.clean import clean_html

from . import config, db, loggers, models, session, utils

# TODO: make me extendable.
# We don't attach this to the feed itself, because in ReRSS world a RSS
# can contain any origin source.
SELECTORS = {r"^https?://(www.)?lemonde.fr": "#articleBody"}

SELECTORS = {
    re.compile(p): HTMLTranslator().css_to_xpath(s) for p, s in SELECTORS.items()
}


async def fetch_content(link):
    for pattern, selector in SELECTORS.items():
        if pattern.match(link):
            async with httpx.AsyncClient() as client:
                resp = await client.get(link)
            doc = fromstring(resp.text)
            try:
                node = doc.xpath(selector)[0]
            except IndexError:
                return
            return clean_html(tostring(node))


def iter_db():
    for sub in Path(config.DATA_ROOT).iterdir():
        if sub.is_dir():
            yield sub.resolve()


def make_opml():
    user = session.username.get()
    body = []
    opml = {
        "opml": {
            "@version": "2.0",
            "head": {
                "title": "My Feeds",
                "dateCreated": utils.utcnow(),
                "ownerName": user,
            },
            "body": body,
        }
    }
    for feed in db.feed.all():
        body.append({"outline": {"@text": feed.title, "@xmlUrl": feed.link}})
    f = Path(config.DATA_ROOT) / user / "opml.xml"
    f.write_text(xmltodict.unparse(opml))


def make_feed():
    user = session.username.get()
    loggers.logger.debug(f"Making feed for {user}")
    feed = {
        "feed": {
            "@xmlns": "http://www.w3.org/2005/Atom",
            "title": f"ReRss of {user}",
            "link": {"@href": f"{config.BASE_URL}/{user}", "@rel": "self"},
            "updated": utils.utcnow().isoformat(),  # Fixme.
            "id": f"tag:{config.DOMAIN},2021:{user}",
            "entry": [],
        }
    }
    entries = db.entry.flagged()
    for entry in entries:
        data = {
            "id": entry.id,
            "title": entry.title,
            "updated": entry.updated.isoformat(),
            "link": {"@href": entry.link},
            "author": entry.author,
            "source": entry.source,
        }
        if entry.summary:
            data["summary"] = {"@type": "html", "#text": entry.summary}
        if entry.content:
            data["content"] = {"@type": "html", "#text": entry.content}
        feed["feed"]["entry"].append(data)
    f = Path(config.DATA_ROOT) / user / "atom.xml"
    f.write_text(xmltodict.unparse(feed))


def bootstrap_for(username, email):
    owner_file = Path(config.DATA_ROOT) / username / "owner"
    if not owner_file.exists():
        owner_file.parent.mkdir(exist_ok=True)
        owner_file.write_text(email)
        with session.as_user(username):
            db.init()
            db.bootstrap()


async def get_feed(link):
    async with httpx.AsyncClient() as client:
        try:
            resp = await client.get(link)
        except httpx.HTTPError:
            return None
        return resp.text


async def fetch(link):
    raw = await get_feed(link)
    if not raw:
        return None  # FIXME
    data = feedparser.parse(raw)
    try:
        feed = db.feed.fetchone("SELECT * FROM feed WHERE link=?", link)
    except db.NoData:
        feed = models.Feed.from_parser(data["feed"])
    feed.synced = utils.utcnow()
    db.feed.insert(feed)
    source = {
        "title": feed.title,
        "subtitle": feed.subtitle,
        "id": feed.id,
        "link": {"@href": feed.link},
    }
    author = data["feed"].get("author_detail")
    if author and "href" in author:
        author["uri"] = author.pop("href")  # Conform to Atom spec.
    for raw in data["entries"]:
        if "link" not in raw:
            loggers.log(f"Invalid entry: {raw}")
            continue
        entry = models.Entry.from_parser(dict(raw))
        entry.setdefault("source", source)
        if author and not entry.author:
            entry.author = dict(author)
        db.entry.insert(entry)
        db.entry_feed.insert(entry, feed)
    return feed
