from functools import wraps
from pathlib import Path
from traceback import print_exc
from urllib.parse import unquote

import ujson as json
from roll import HttpError, Roll
from roll.extensions import cors, options, traceback

from . import config, db, emails, helpers, loggers, models, session, tokens


app = Roll()
options(app)
traceback(app)


@app.listen("startup")
async def on_startup():
    cors(
        app,
        methods="*",
        headers=["content-type"],
        origin=config.ALLOW_ORIGIN,
        credentials=True,
    )


@app.listen("error")
async def json_error_response(request, response, error):
    if error.status == 404:
        error.message = {"error": f"Path not found `{request.path}`"}
    if error.status == 500:  # This error as not been caught yet
        if isinstance(error.__context__, db.NoData):
            response.status = 404
            error.message = f"Resource not found: {error.__context__}"
        elif isinstance(error.__context__, ValueError):
            response.status = 422
            loggers.logger.error(str(error.__context__))
        else:
            print_exc()
    if isinstance(error.message, (str, bytes)):
        error.message = {"error": error.message}
    response.json = error.message


def with_session(view):
    @wraps(view)
    def wrapper(req, resp, *args, **kwargs):
        session.username.set(req["username"])
        db.init()
        return view(req, resp, *args, **kwargs)

    return wrapper


@app.route("/token", methods=["POST"])
async def send_token(req, resp):
    # TODO mailbomb management in nginx
    email = req.json.get("email")
    username = req.json.get("username")
    if not email:
        raise HttpError(400, "Missing email key")
    if not username:
        raise HttpError(400, "Missing username key")
    owner_file = Path(config.DATA_ROOT) / username / "owner"
    if owner_file.exists() and owner_file.read_text() != email:
        raise HttpError(403, "Unauthorized")
    redirect = req.json.get("referer", req.referer)
    token = tokens.create(username, email)
    link = f"{req.host}/login/{token}?redirect={redirect}"
    if "localhost" in link or "127.0.0.1" in link:
        link = f"http://{link}"
        print(link)
    else:
        link = f"https://{link}"
    body = emails.ACCESS_GRANTED.format(link=link, username=username)
    emails.send(email, "rerss access", body)
    resp.status = 204


@app.route("/login/{token}")
async def login(req, resp, token):
    try:
        username, email = tokens.read(token)
    except ValueError:
        raise HttpError(403, "Invalid token")
    redirect = req.query.get("redirect")
    if not redirect:
        raise HttpError(400, "Missing redirect key")
    helpers.bootstrap_for(username, email)
    resp.cookies.set(name="api-key", value=token)
    resp.redirect = redirect, 302


@app.route("/sync", protocol="websocket")
@tokens.require
@with_session
async def sync(req, ws):
    feeds = list(db.feed.all())
    total = len(feeds)
    done = 0
    await ws.send(json.dumps({"progress": done / total}))
    for feed in feeds:
        await helpers.fetch(feed["link"])
        done += 1
        await ws.send(json.dumps({"progress": done / total}))
    await ws.send(json.dumps({"status": "done"}))


@app.route("/feed")
@tokens.require
@with_session
async def feeds(req, resp):
    resp.json = {"feeds": [dict(f) for f in db.feed.all()]}


@app.route("/feed", methods=["POST"])
@tokens.require
@with_session
async def new_feed(req, resp):
    feed = await helpers.fetch(req.json["link"])
    resp.json = feed


# TODO: merge with above route and use PUT instead ?
@app.route("/feed/{link:path}", methods=["GET", "POST"])
@tokens.require
@with_session
async def feed(req, resp, link):
    feed = db.feed.fetchone("SELECT * FROM feed WHERE link=?", link)
    if req.method == "POST":
        for key, value in req.json.items():
            feed[key] = value
        db.feed.insert(feed)
        if link != feed.link:
            await helpers.fetch(feed["link"])
    resp.json = dict(feed)


@app.route("/feed/{link:path}", methods=["DELETE"])
@tokens.require
@with_session
async def delete_feed(req, resp, link):
    db.feed.delete(link)
    resp.status = 204


@app.route("/entry")
@tokens.require
@with_session
async def entries(req, resp):
    limit = req.query.int("limit", 100)
    offset = req.query.int("offset", 0)
    where = []
    params = []
    seen = req.query.bool("seen", None)
    if seen is not None:
        where.append("seen=?")
        params.append(seen)
    bookmark = req.query.bool("bookmark", None)
    if bookmark is not None:
        where.append("bookmark=?")
        params.append(bookmark)
    feed = req.query.get("feed", None)
    if feed is not None:
        where.append("entry_feed.feed=?")
        params.append(feed)
    qs = db.entry.select(where=where, params=params, limit=limit, offset=offset)
    resp.json = {"entries": [dict(entry) for entry in qs]}


@app.route("/entry/{link:path}", methods=["POST"])
@tokens.require
@with_session
async def update_entry(req, resp, link):
    link = unquote(link)
    data = req.json
    data["link"] = link
    entry = db.entry.insert(models.Entry(data))
    if "flagged" in data:
        helpers.make_feed()
    resp.json = entry


@app.route("/mark-all-seen", methods=["POST"])
@tokens.require
@with_session
async def mark_all_seen(req, resp):
    cursor = db.entry.execute("UPDATE entry SET seen=true WHERE seen=false")
    resp.json = {"updated": cursor.rowcount}


# TODO: remove this and replace with a statically generated file served by nginx.
@app.route("/{user}/atom.xml")
async def myfeed(req, resp, user):
    path = Path(config.DATA_ROOT) / user / "atom.xml"
    path = path.resolve()
    if Path(config.DATA_ROOT).resolve() not in path.parents:
        raise HttpError(400, f"Invalid user {user}")
    if not path.exists():
        raise HttpError(404, "Feed not found")
    resp.body = path.read_text()
    resp.headers["Content-Type"] = "application/xml"
