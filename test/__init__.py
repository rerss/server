from pathlib import Path


from rerss import config, helpers, loggers

DATA_ROOT = Path(__file__).parent / "tmp/db"

config.DATA_ROOT = DATA_ROOT
config.init()
loggers.init()
for path in helpers.iter_db():
    for file in path.iterdir():
        file.unlink()
