import asyncio
from datetime import datetime, timezone
from unittest.mock import patch

from ward import fixture
from roll.testing import Client as BaseClient

from rerss import config, db, helpers, models, session, tokens
from rerss.app import app

DATE = datetime(2021, 6, 22, 15, 32, 21, tzinfo=timezone.utc)
FEED = """<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
    <title>Foo Bar</title>
    <subtitle>Dernières mises à jour du site</subtitle>
    <link href="https://foobar.fr/feed/" rel="self" />
    <id>https://foobar.fr/feed/</id>
    <updated>2021-06-24T12:00:00+01:00</updated>
    <author>
        <name>Foo Bar</name>
        <uri>https://foobar.fr/foobar/</uri>
    </author>
    <rights>Copyright (c) 2004-2021, Foo Bar</rights>

        <entry xml:lang="fr">
            <title type="html">Title</title>
            <link href="https://foobar.fr/foobar/2021/06/17/" rel="alternate" type="text/html" />
            <updated>2021-06-17T12:00:00+01:00</updated>
            <id>https://foobar.fr/foobar/2021/06/17/</id>
            <summary type="html">&lt;p&gt;Foobaréœ&lt;/p&gt;</summary>
            <content type="html">&lt;p&gt;Content&lt;/p&gt;</content>
        </entry>

        <entry xml:lang="fr">
            <title type="html">Title 2</title>
            <link href="https://foobar.fr/foobar/2021/06/16/" rel="alternate" type="text/html" />
            <updated>2021-06-16T12:00:00+01:00</updated>
            <id>https://foobar.fr/foobar/2021/06/16/</id>
            <summary type="html">&lt;p&gt;FooFooéœ&lt;/p&gt;</summary>
            <content type="html">&lt;p&gt;Content&lt;/p&gt;</content>
        </entry>

</feed>"""
INCREMENT = 1


def increment():
    global INCREMENT
    INCREMENT += 1
    return INCREMENT


class Client(BaseClient):
    def login(self, username, email):
        token = tokens.create(username, email)
        self.default_headers["API-Key"] = token
        return token

    def logout(self):
        try:
            del self.default_headers["API-Key"]
        except KeyError:
            pass


@fixture
def database():
    for path in helpers.iter_db():
        db_path = path / "rerss.db"
        if db_path.exists():
            with session.as_user(path.name):
                db.init()
                db.table.execute("DELETE FROM feed")
                db.table.execute("DELETE FROM entry")
                db.table.execute("DELETE FROM entry_feed")


@fixture
def patch_feed():
    with patch.object(helpers, "get_feed") as mocked:
        yield mocked


@fixture(scope="global")
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop
    loop.close()


@fixture
def client(event_loop=event_loop, database=database):
    app.loop = event_loop
    app.loop.run_until_complete(app.startup())
    yield Client(app)
    app.loop.run_until_complete(app.shutdown())


@fixture
def user(client=client):
    client.login("foobar", "foo@bar.org")
    path = config.DATA_ROOT / "foobar" / "rerss.db"
    path.parent.mkdir(exist_ok=True)
    with session.as_user("foobar"):
        db.init()
        db.bootstrap()
        yield
        client.logout()


@fixture
def feed(database=database):
    def _(**kwargs):
        default = {
            "id": f"tag:rerss.org,2005-11-09:/feed.xml,{increment()}",
            "title": "Test Feed",
            "subtitle": "Very subtile",
            "link": "https://local.host/feed",
            "updated": DATE,
            "synced": DATE,
        }
        default.update(kwargs)
        feed = models.Feed(default)
        db.feed.insert(feed)
        return feed

    return _


@fixture
def entry(database=database, make_feed=feed):
    def _(feed=None, **kwargs):
        default = {
            "id": f"tag:rerss.org,2005-11-09:atom.xml:{increment()}",
            "title": "bar",
            "summary": "bar",
            "content": "bar",
            "link": "http://foo.bar",
            "image": "https://foo.bar/img.png",
            "updated": DATE,
            "flagged": False,
            "seen": False,
            "bookmark": False,
            "author": {"name": "Ba Bar", "href": "https://ba.bar"},
            "source": {
                "title": "title",
                "link": "https://source.org/feed",
                "id": "123",
            },
            "raw": {"title": "raw title"},
        }
        default.update(kwargs)
        entry = models.Entry(default)
        db.entry.insert(entry)
        db.entry_feed.insert(entry, feed or make_feed())
        return entry

    return _
