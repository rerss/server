from pathlib import Path
from unittest.mock import patch

from ward import test

from rerss import config, db, helpers, utils

from test.fixtures import patch_feed, FEED, entry, feed, user, DATE


@test("helpers.bootstrap_for should create user space")
def _():
    helpers.bootstrap_for("foofoo", "foo@foo.foo")
    path = config.DATA_ROOT / "foofoo" / "owner"
    assert path.exists()
    assert path.read_text() == "foo@foo.foo"


@test("helpers.fetch should fetch remote feed and create Feed and Entry")
async def _(patch_feed=patch_feed, user=user):
    patch_feed.return_value = FEED
    await helpers.fetch("https://foobar.fr/feed/")
    total = db.feed.fetchval("SELECT COUNT(*) as total FROM feed")
    assert total == 1
    feed = db.feed.fetchone("SELECT * FROM feed WHERE link='https://foobar.fr/feed/'")
    assert feed.synced
    assert feed.updated
    assert feed.active is True
    assert feed.failed is None
    assert feed.id == "https://foobar.fr/feed/"
    assert feed.title == "Foo Bar"
    assert feed.subtitle == "Dernières mises à jour du site"
    total = db.feed.fetchval("SELECT COUNT(*) as total FROM entry")
    assert total == 2
    entry = db.entry.fetchone(
        "SELECT * FROM entry WHERE link='https://foobar.fr/foobar/2021/06/16/'"
    )
    assert entry.raw
    assert entry.bookmark is False
    assert entry.flagged is False
    assert entry.seen is False
    assert entry.id == "https://foobar.fr/foobar/2021/06/16/"
    assert entry.link == "https://foobar.fr/foobar/2021/06/16/"
    assert entry.source == {
        "id": "https://foobar.fr/feed/",
        "link": {"@href": "https://foobar.fr/feed/"},
        "subtitle": "Dernières mises à jour du site",
        "title": "Foo Bar",
    }
    assert entry.summary == "<p>FooFooéœ</p>"
    assert entry.content == "<p>Content</p>"
    assert entry.updated
    assert entry.title == "Title 2"
    assert entry.author == {"name": "Foo Bar", "uri": "https://foobar.fr/foobar/"}


@test("helpers.fetch should not override changes on feed local fields")
async def _(patch_feed=patch_feed, user=user):
    patch_feed.return_value = FEED
    link = "https://foobar.fr/feed/"
    await helpers.fetch(link)
    # Use has changed the title.
    db.entry.execute("UPDATE feed SET title='NEW TITLE' WHERE link=?", link)
    await helpers.fetch(link)
    title = db.entry.fetchval("SELECT title FROM feed WHERE link=?", link)
    assert title == "NEW TITLE"


@test("helpers.fetch should not override changes on entry local fields")
async def _(patch_feed=patch_feed, user=user):
    patch_feed.return_value = FEED
    await helpers.fetch("https://foobar.fr/feed/")
    seen = db.entry.fetchval(
        "SELECT seen FROM entry WHERE link='https://foobar.fr/foobar/2021/06/16/'"
    )
    assert seen is False
    db.entry.execute(
        "UPDATE entry SET seen=true WHERE link='https://foobar.fr/foobar/2021/06/16/'"
    )
    # Sync feed again, but an entry title has been changed
    patch_feed.return_value = FEED.replace(
        '<title type="html">Title 2</title>', '<title type="html">NEW TITLE</title>'
    )
    await helpers.fetch("https://foobar.fr/feed/")
    title = db.entry.fetchval(
        "SELECT title FROM entry WHERE link='https://foobar.fr/foobar/2021/06/16/'"
    )
    assert title == "NEW TITLE"
    seen = db.entry.fetchval(
        "SELECT seen FROM entry WHERE link='https://foobar.fr/foobar/2021/06/16/'"
    )
    assert seen is True


@test("helpers.fetch should not override original entry.source")
async def _(patch_feed=patch_feed, feed=feed, entry=entry, user=user):
    aggregated = """<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
    <title>Some ReRSS</title>
    <subtitle>Aggregated Feed</subtitle>
    <link href="https://aggregated.fr/feed/" rel="self" />
    <id>https://aggregated.fr/feed/</id>
    <updated>2021-06-24T12:00:00+01:00</updated>
    <author>
        <name>Foo Bar</name>
        <uri>https://foobar.fr/foobar/</uri>
    </author>
    <rights>Copyright (c) 2004-2021, Foo Bar</rights>

        <entry xml:lang="fr">
            <title type="html">Title</title>
            <link href="https://foobar.fr/foobar/2021/06/17/" rel="alternate" type="text/html" />
            <updated>2021-06-17T12:00:00+01:00</updated>
            <id>https://foobar.fr/foobar/2021/06/17/</id>
            <summary type="html">&lt;p&gt;Foobar&lt;/p&gt;</summary>
            <source>
                <title>Foo Bar</title>
                <subtitle>Dernières mises à jour du site</subtitle>
                <link href="https://foobar.fr/feed/" rel="self" />
                <id>https://foobar.fr/feed/</id>
            </source>
        </entry>

</feed>"""
    # First import original feed
    patch_feed.return_value = FEED
    await helpers.fetch("https://foobar.fr/feed/")
    link = "https://foobar.fr/foobar/2021/06/16/"
    source = db.entry.fetchval("SELECT source FROM entry WHERE link=?", link)
    assert source == {
        "id": "https://foobar.fr/feed/",
        "link": {"@href": "https://foobar.fr/feed/"},
        "subtitle": "Dernières mises à jour du site",
        "title": "Foo Bar",
    }
    sources = db.entry_feed.fetchval(
        "SELECT COUNT(*) FROM entry_feed WHERE entry=?", link
    )
    assert sources == 1
    # Now we import an aggregated feed that contain an entry from the original one.
    # It also contain the original source (same link).
    patch_feed.return_value = aggregated
    await helpers.fetch("https://aggregated.fr/feed/")
    source = db.entry.fetchval(
        "SELECT source FROM entry WHERE link='https://foobar.fr/foobar/2021/06/16/'"
    )
    assert source == {
        "id": "https://foobar.fr/feed/",
        "link": {"@href": "https://foobar.fr/feed/"},
        "subtitle": "Dernières mises à jour du site",
        "title": "Foo Bar",
    }


@test("helpers.make_opml should create opml files")
async def _(user=user, feed=feed):
    feed(title="My Feed", link="http://feed.link")
    with patch.object(utils, "utcnow", return_value=DATE):
        helpers.make_opml()
    output = Path(config.DATA_ROOT / "foobar/opml.xml").read_text()
    assert output == (
        """<?xml version="1.0" encoding="utf-8"?>\n"""
        """<opml version="2.0"><head><title>My Feeds</title>"""
        """<dateCreated>2021-06-22 15:32:21+00:00</dateCreated>"""
        """<ownerName>foobar</ownerName></head><body>"""
        """<outline text="My Feed" xmlUrl="http://feed.link"></outline></body></opml>"""
    )
