import json
from unittest.mock import patch

from ward import test

from rerss import db, utils

from test.fixtures import client, feed, user, patch_feed, FEED, DATE, entry


@test("/feed should return feeds")
async def _(client=client, user=user, feed=feed):
    feed(title="My Feed", id="tag:rerss.org,2005-11-09:/feed.xml")
    resp = await client.get("/feed")
    assert resp.status == 200
    assert json.loads(resp.body) == {
        "feeds": [
            {
                "id": "tag:rerss.org,2005-11-09:/feed.xml",
                "active": True,
                "failed": None,
                "link": "https://local.host/feed",
                "subtitle": "Very subtile",
                "synced": 1624375941,
                "title": "My Feed",
                "updated": 1624375941,
            },
        ]
    }


@test("POST on /feed should create a feed")
async def _(client=client, patch_feed=patch_feed, user=user):
    patch_feed.return_value = FEED
    with patch.object(utils, "utcnow", return_value=DATE):
        resp = await client.post("/feed", body={"link": "https://local.host/feed"})
    assert resp.status == 200
    assert json.loads(resp.body) == {
        "link": "https://foobar.fr/feed/",
        "id": "https://foobar.fr/feed/",
        "subtitle": "Dernières mises à jour du site",
        "synced": 1624375941,
        "title": "Foo Bar",
        "updated": 1624528800,
        "active": True,
        "failed": None,
    }


@test("DELETE on /feed should delete a feed")
async def _(client=client, feed=feed, user=user):
    link = "https://local.host/feed"
    feed(link=link)
    feed(link="http://another.link", id="tag:another")
    assert db.feed.fetchval("SELECT COUNT(*) FROM feed") == 2
    resp = await client.delete(f"/feed/{link}")
    assert resp.status == 204
    assert db.feed.fetchval("SELECT COUNT(*) FROM feed") == 1
    assert db.feed.fetchval("SELECT COUNT(*) FROM feed WHERE link='{link}'") == 0


@test("/entry should retrieve entries")
async def _(entry=entry, client=client, user=user, feed=feed):
    entry(title="New", link="http://f.fr/new")
    entry(title="Seen", seen=True, link="http://f.fr/seen")
    entry(title="Bookmark", bookmark=True, link="http://f.fr/bookmark")
    entry(
        title="By Link",
        link="http://another.link/entry",
        feed=feed(link="http://another.link"),
    )
    resp = await client.get("/entry")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["entries"]) == 4

    resp = await client.get("/entry?seen=true")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["entries"]) == 1
    assert data["entries"][0]["title"] == "Seen"

    resp = await client.get("/entry?bookmark=true")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["entries"]) == 1
    assert data["entries"][0]["title"] == "Bookmark"

    resp = await client.get("/entry?feed=http://another.link")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["entries"]) == 1
    assert data["entries"][0]["title"] == "By Link"


@test("POST on /entry/xxx should update entry and return object")
async def _(entry=entry, feed=feed, client=client, user=user):
    link = "http://another.link/123"
    entry(
        title="Entry",
        link=link,
        feed=feed(link="http://another.link", id="tag:feed,2021:123"),
        id="tag:rerss.org,2005-11-09:atom.xml:20x",
    )
    resp = await client.post(f"/entry/{link}", {"seen": True})
    assert resp.status == 200
    assert json.loads(resp.body) == {
        "author": {"href": "https://ba.bar", "name": "Ba Bar"},
        "bookmark": False,
        "content": "bar",
        "flagged": False,
        "id": "tag:rerss.org,2005-11-09:atom.xml:20x",
        "image": "https://foo.bar/img.png",
        "link": link,
        "source": {"id": "123", "link": "https://source.org/feed", "title": "title"},
        "raw": {"title": "raw title"},
        "seen": True,
        "sources": [
            {
                "active": True,
                "failed": None,
                "id": "tag:feed,2021:123",
                "link": "http://another.link",
                "subtitle": "Very subtile",
                "synced": 1624375941,
                "title": "Test Feed",
                "updated": 1624375941,
            },
        ],
        "summary": "bar",
        "title": "Entry",
        "updated": 1624375941,
    }
    seen = db.entry.fetchval("SELECT seen FROM entry WHERE link=?", link)
    assert seen is True
