from unittest.mock import patch

from ward import test

from rerss.helpers import make_feed
from rerss import utils

from test.fixtures import client, entry, user, DATE


@test("unknown feed should return a 404")
async def _(client=client):
    resp = await client.get("/unknown/atom.xml")
    assert resp.status == 404


@test("make_feed should create atom")
async def test_empty_feed(client=client, user=user, entry=entry):
    entry(flagged=True, id="tag:rerss.org")
    with patch.object(utils, "utcnow", return_value=DATE):
        make_feed()
    resp = await client.get("/foobar/atom.xml")
    assert resp.status == 200
    print(resp.body)
    assert resp.body == (
        b"""<?xml version="1.0" encoding="utf-8"?>\n"""
        b"""<feed xmlns="http://www.w3.org/2005/Atom"><title>ReRss of foobar</title>"""
        b"""<link href="http://0.0.0.0:1707/foobar" rel="self"></link>"""
        b"""<updated>2021-06-22T15:32:21+00:00</updated>"""
        b"""<id>tag:rerss.org,2021:foobar</id>"""
        b"""<entry><id>tag:rerss.org</id><title>bar</title>"""
        b"""<updated>2021-06-22T15:32:21+00:00</updated>"""
        b"""<link href="http://foo.bar"></link>"""
        b"""<author><name>Ba Bar</name><href>https://ba.bar</href></author>"""
        b"""<source><title>title</title><link>https://source.org/feed</link>"""
        b"""<id>123</id></source>"""
        b"""<summary type="html">bar</summary><content type="html">bar</content>"""
        b"""</entry></feed>"""
    )
