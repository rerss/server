from unittest.mock import patch

from ward import test

from rerss import config, emails, tokens

from test.fixtures import client


@test("Call to /token should send an email with the token")
async def _(client=client):
    client.logout()
    with patch.object(emails, "send") as mocked:
        resp = await client.post(
            "/token", body={"email": "foo@bar.org", "username": "foo"}
        )
    assert resp.status == 204
    assert mocked.call_count == 1
    to, subject, body = mocked.call_args.args
    assert to == "foo@bar.org"
    assert "/login/" in body


@test("/login should create user space")
async def _(client=client):
    token = tokens.create("foofoo", "foo@foo.foo")
    resp = await client.get(f"/login/{token}?redirect=http://frontend")
    assert resp.status == 302
    assert resp.headers["Location"] == "http://frontend"
    path = config.DATA_ROOT / "foofoo" / "owner"
    assert path.exists()
    assert path.read_text() == "foo@foo.foo"
