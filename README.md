# ReRSS Python server

## Install

- create a venv:

  python -m venv path/to/venv

- activate it:

  source path/to/venv/bin/activate

- install

  python setup.py develop

## Run locally

Make sure your venv is activated, then:

    rerss initdb
    rerss serve

Optionally logs the requests:

    rerss serve --log

Or autoreload the server when you change a python file (needs `hupper` to be
installed):

    rerss serve --reload

Then browse to http://localhost:1707/app/index.html

To allow CORS, you need to provide the RERSS_ALLOW_ORIGIN env variable when starting the server:

RERSS_ALLOW_ORIGIN=http://localhost:3000 rerss serve

## Import feeds from an OPML file

If you have an [OPML file](https://en.wikipedia.org/wiki/OPML) exported from a
previous RSS reader, you can import all your feeds using the cli:

    rerss import_opml /path/to/the/file.opml
